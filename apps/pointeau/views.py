import json
from django.http import HttpResponse, Http404
from django.shortcuts import render_to_response
from apps.pointeau.models import Point_eau

def list(request):
    jsonliste = [pointeau.json() for pointeau in Point_eau.objects.all()]
    return HttpResponse(json.dumps(jsonliste), content_type='application/json')

def update(request):
    print request.POST['jsondata']
    if request.method == 'POST':
        for jsonplage in json.loads(request.POST['jsondata']):
            pointeau = Point_eau.objects.get_or_create(jsonplage['id'])
            pointeau.lat = jsonplage['lat']
            pointeau.lng = jsonplage['lng']
            pointeau.qualite = jsonplage['qualite']
            pointeau.save()
        return HttpResponse('OK', mimetype='text/plain')
    else:
        return Http404('Only as POST')