"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

from django.test import TestCase
from django.test.client import Client
from apps.pointeau.models import Point_eau


class SimpleTest(TestCase):
    def setUp(self):
        self.plage = Point_eau.objects.create(lat=34.55, lng=45.65, qualite=3)

    def testpostdata(self):
        c = Client()
        response = c.post('/plages/update/',
            {'jsondata' :
                [{'id': 1, 'lat' : 45.56, 'lng' : 34.54, 'qualite' : 4},
                {'id': 2, 'lat' : 23.54, 'lng' : 54.65, 'qualite' : 3}]
            }
        )
        print response
#        self.assertEqual(response.status_code, 200)
#        print 'reponse: '+response.content
#        response = c.get('/plages')
#        print 'contenu: ' + response.content
#        self.assertEqual(response.content.id, self.plage.id)
#        self.assertNotEqual(response.content.qualite, self.plage.qualite)

