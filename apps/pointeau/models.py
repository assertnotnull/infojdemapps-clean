from django.db import models

class Point_eau(models.Model):
    lat = models.FloatField()
    lng = models.FloatField()
    qualite = models.IntegerField()

    def json(self):
        fields = ('lat', 'lng', 'qualite', 'id')
        return dict((field, self.__dict__[field]) for field in fields)

    def __unicode__(self):
        return str(self.lat) + ', ' + str(self.lng) + ": " + str(self.qualite)