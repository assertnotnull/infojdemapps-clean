from django.conf.urls.defaults import patterns, url

urlpatterns = patterns('apps.pointeau.views',
    url(r'^pointeau/$', 'list', name='list'),
    url(r'^pointeau/update/$', 'update', name='update'),
)