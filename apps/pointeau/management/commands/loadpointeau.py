from django.core.management.base import BaseCommand
from apps.pointeau.models import Point_eau

class Command(BaseCommand):
    help = 'Load csv data of postal codes as: code,district_id'
    option_list = BaseCommand.option_list

    def get_version(self):
        return '1'

    def handle(self, *args, **options):
        file = open('apps/pointeau/pointeau.csv', 'r')
        size = len(file.readlines())
        file.seek(0)
        i = 0.0
        file.readline()
        for value in file.readlines():
            i += 1
            values = value.split(',')
            Point_eau.objects.create(
                lat=float(values[0]),
                lng=float(values[1]),
                qualite=int(values[2])
            )