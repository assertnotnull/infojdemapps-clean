# -=- encoding: utf-8 -=-
from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from taggit.managers import TaggableManager

class Tag(models.Model):
    nom_court = models.SlugField(max_length=30, unique=True)
    nom = models.CharField(max_length=30)

    def __unicode__(self):
        return self.nom

class Nouvelle(models.Model):
    id = models.BigIntegerField(unique=True,primary_key=True)
    revision_date = models.DateTimeField(u'date de révision')
    title = models.CharField('titre',max_length=100)
    content_snippet = models.TextField('contenu',max_length=500)
    link = models.CharField('lien',max_length=150)
    published_date = models.DateTimeField('date de publication')
    lat = models.FloatField('latitude')
    lng = models.FloatField('longitude')
    tags = models.CharField(max_length=40)

    def __unicode__(self):
        return self.title

    def setTags(self, tagline):
        self.tags.clear()
        for tag in tagline.split(','):
            try:
                tagitem, status = Tag.objects.get_or_create(nom_court=tag, nom=tag)
            except ObjectDoesNotExist, e:
                tagitem = Tag(nom_court=tag, nom=tag)
                tagitem.save()
            self.tags.add(tagitem)

    def setTagsList(self, tags):
        self.tags.clear()
        for tag in tags:
            try:
                tagitem, status = Tag.objects.get_or_create(nom_court=tag, nom=tag)
            except ObjectDoesNotExist, e:
                tagitem = Tag(nom_court=tag, nom=tag)
                tagitem.save()
            self.tags.add(tagitem)

