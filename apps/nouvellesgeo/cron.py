import base64
from django_cron import CronJobBase, Schedule
from urllib2 import urlopen
import urllib2
from datetime import datetime
from django.core.exceptions import ObjectDoesNotExist
from django.core.serializers import json
from apps.nouvellesgeo.models import Nouvelle

class UpdateNews(CronJobBase):
    url = 'http://www.journaldemontreal.com/dossier/elections2012?overridetemplate=SUN_PAGE_RSS_ALL'
    RUN_EVERY_MINS = 60 #chaque heure
    schedule = Schedule(run_every_mins=RUN_EVERY_MINS)
    code = 'nouvellesgeo.cron'
    TIME_FORMAT = '%a %b %d %H:%M:%S %Z %Y'

    id = 'id'
    title = 'title'
    content = 'contentSnippet'
    link = 'link'
    published_date = 'publishedDate'
    revision_date = 'revisionDate'
    lat = 'lat'
    lng = 'lng'
    tags = 'tags'

    def do(self):
        req = urllib2.Request(self.url)
#        base64string = base64.encodestring(
#            '%s:%s' % ('Formation', 'Bonjour4$'))[:-1]
#        authheader =  "Basic %s" % base64string
#        req.add_header("Authorization", authheader)

        data = urlopen(req)
        content = data.read().decode('utf-8').replace('jsoncallback(','').replace('})','}')
        data.close()
        jsondata = json.simplejson.loads(content)
        print 'Importing news from feed'
        for jsonnouvelle in jsondata['TopicStories']:
            if jsonnouvelle.get(self.lat) is not None or jsonnouvelle.get(self.lng) is not None:
                try:
                    nouvelle = Nouvelle.objects.get(id=jsonnouvelle[self.id])
                except ObjectDoesNotExist, e:
                    nouvelle = Nouvelle(id=jsonnouvelle[self.id])
                nouvelle.title = jsonnouvelle[self.title]
                nouvelle.content_snippet = jsonnouvelle[self.content]
                nouvelle.link = jsonnouvelle[self.link]
                nouvelle.published_date = datetime.strptime(jsonnouvelle[self.published_date], self.TIME_FORMAT)
                nouvelle.revision_date = datetime.strptime(jsonnouvelle[self.revision_date], self.TIME_FORMAT)
                nouvelle.lat = jsonnouvelle[self.lat]
                nouvelle.lng = jsonnouvelle[self.lng]
                nouvelle.tags = ','.join(jsonnouvelle[self.tags])
#                nouvelle.setTagsList(jsonnouvelle[self.tags])
                nouvelle.save()
        print 'Done importing news'
