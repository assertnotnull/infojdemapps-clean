import datetime
from django.core.urlresolvers import reverse

from django.test import TestCase
from django.test.client import Client
from apps.nouvellesgeo.cron import UpdateNews
from apps.nouvellesgeo.models import Nouvelle, Tag


class WhenICreateANews(TestCase):
    def setUp(self):
        self.nouvelle = Nouvelle.objects.create(
            id=1,
            date_revision=datetime.datetime(2012, 2, 3, 15, 22, 23),
            titre='titre',
            contenu='texte',
            lien='www.gog.com',
            date_publication=datetime.datetime(2012, 2, 2, 15, 10, 11),
            lat=-75.456767,
            lng=45.7668778,
        )
        self.tag1 = Tag(nom_court='PI', nom='PI')
        self.tag1.save()
        self.tag2 = Tag(nom_court='PQ', nom='PQ')
        self.tag2.save()
        self.nouvelle.tags.add(self.tag1, self.tag2)
        self.nouvelle.save()
        self.client = Client()

    def testWithSuccess(self):
        data = {
            'id':123435,
            'revisioned_date': datetime.datetime(2012, 2, 3, 15, 22, 23),
            'title': 'titre2',
            'contentSnippet': 'texte2',
            'link': 'www.google.com',
            'published_date': datetime.datetime(2012, 2, 2, 15, 10, 11),
            'lat': -75.5465,
            'lng': 45.756775,
            'tags': 'PI,PQ'
        }
        response = self.client.post('nouvellesgeo/nouvelle/?format=json')
        print response.content
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'gog')

class WhenIUpdateANews(TestCase):
    def setUp(self):
        self.nouvelle = Nouvelle.objects.create(
            id=1,
            date_revision=datetime.datetime(2012, 2, 3, 15, 22, 23),
            titre='titre',
            contenu='texte',
            lien='www.gog.com',
            date_publication=datetime.datetime(2012, 2, 2, 15, 10, 11),
            lat=-75.456767,
            lng=45.7668778,
        )
        self.tag = Tag(nom_court='PQ', nom='PQ')
        self.nouvelle.tags.add(self.tag)
        self.nouvelle.save()
        self.client = Client()

    def testNotFound(self):
        data = {
            'date_revision': datetime.datetime(2012, 2, 3, 15, 22, 23),
            'titre': 'titre2',
            'contenu': 'texte2',
            'lien': 'www.google.com',
            'date_publication': datetime.datetime(2012, 2, 2, 15, 10, 11),
            'lat': -75.5465,
            'lng': 45.756775,
            'tags':'PI'
        }
        response = self.client.post(reverse('nouvelle_url', kwargs={'id': 10}), data)