from tastypie.resources import ModelResource
from tastypie import fields
from apps.nouvellesgeo.models import Nouvelle, Tag

class TagResource(ModelResource):
    class Meta:
        resource_name = 'tags'
        queryset = Tag.objects.all()
        excludes = ['id','nom_court']
        include_resource_uri = False


class NouvelleResource(ModelResource):
#    tags = fields.ToManyField(TagResource, 'tags', full=True)
    class Meta:
        queryset = Nouvelle.objects.all()
        resource_name = 'nouvellesgeo/nouvelle'
        list_allowed_methods = ['get']
        excludes = ['id']
        ordering = ['id']
        default_format='application/json'
        filtering = {
            'published_date' : ('gte', 'exact')
        }

    def dehydrate_tags(self, bundle):
        return bundle.obj.tags.split(',')
