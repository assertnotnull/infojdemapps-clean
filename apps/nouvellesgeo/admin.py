from apps.nouvellesgeo.models import Nouvelle, Tag
from django.contrib import admin

admin.site.register(Nouvelle)
admin.site.register(Tag)