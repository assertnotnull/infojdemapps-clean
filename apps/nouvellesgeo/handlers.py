from piston.handler import AnonymousBaseHandler#, BaseHandler
import  logging
from django.core.paginator import Paginator
from apps.nouvellesgeo.models import Nouvelle

class NouvelleHandler(AnonymousBaseHandler):
    logger = logging.getLogger(__name__)
    allowed_methods = ('GET', 'PUT', 'POST')
    fields = ('titre', 'contenu', 'lat', 'lng', 'lien', 'date_publication', 'date_revision',('tags', ('name', )))
    #exclude = ('id', re.compile(r'^private_'))
    model = Nouvelle

    def read(self, request, id=None):
        self.logger.info('Getting object with id: %s' % id)
        if id is None:
            paginator = Paginator(Nouvelle.objects.all())
            nouvelle = Nouvelle.objects.all()
        else:
            nouvelle = Nouvelle.objects.get(id=id)
        return nouvelle

    def update(self, request, id):
        self.logger.debug('Updating nouvelle with id: %s' % id)
        nouvelle = Nouvelle.objects.get(id=id)
        texte = request.POST['contentSnippet']
        lat = request.POST['lat']
        lng = request.POST['lng']
        tags = request.POST['tags'].split(',')
        link = request.POST['link']
        published_date = request.POST['published_date']
        title = request.POST['title']
        revisioned_date = request.POST['revisioned_date']

        nouvelle.contenu = texte
        nouvelle.date_publication = published_date
        nouvelle.date_revision = revisioned_date
        nouvelle.lat = lat
        nouvelle.lng = lng
        nouvelle.lien = link
        nouvelle.titre = title
        for tag in tags:
            if len(tag) is not 0:
                nouvelle.tags.add(tag)
        nouvelle.save()
        return nouvelle

    def create(self, request):
        print 'HERE'
        texte = request.POST['contentSnippet']
        lat = request.POST['lat']
        lng = request.POST['lng']
        tags = request.POST['tags'].split(',')
        link = request.POST['link']
        published_date = request.POST['published_date']
        title = request.POST['title']
        revisioned_date = request.POST['revisioned_date']

        print 'again'
        self.logger.debug('Creating nouvelle')
        nouvelle = Nouvelle.objects.create(
            id=id,
            date_revision=revisioned_date,
            titre=title,
            contenu=texte,
            lien=link,
            date_publication=published_date,
            lat=lat,
            lng=lng,
        )
        print 'yay'
        print link
        for tag in tags:
            if len(tag) is not 0:
                nouvelle.tags.add(tag)
        self.logger.debug('Created nouvelle id: %s' % nouvelle.id)
        return nouvelle