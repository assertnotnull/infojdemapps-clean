from django.conf.urls.defaults import patterns, url, include
#from piston.resource import Resource
#from piston.authentication import AnonymousBaseHandler
from apps.nouvellesgeo.api import NouvelleResource

nouvelle_resource = NouvelleResource()

urlpatterns = patterns('',
#    url(r'^nouvelles/(?P<id>[^/]+)/$', nouvelle_resource, name='nouvelle_id_url'),
    url(r'', include(nouvelle_resource.urls), name='nouvelle_url'),
#    url(r'^other/(?P<username>[^/]+)/(?P<data>.+)/$', arbitrary_resource),
)