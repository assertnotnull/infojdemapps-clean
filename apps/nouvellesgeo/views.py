import logging
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from apps.nouvellesgeo.models import Nouvelle

logger = logging.getlogger(__name__)

def add_a_news(request):
    if request.method == 'POST':
        logger.info('test')
        texte = request.POST['texte']
        lat = request.POST['lat']
        lng = request.POST['lng']
        tags = request.POST['tags'].split(',')
        link = request.POST['link']
        published_date = request.POST['published_date']
        title = request.POST['title']
        revisioned_date = request.POST['revisioned_date']

        Nouvelle.objects.create(
            id=id,
            date_revision=revisioned_date,
            titre=title,
            contenu=texte,
            lien=link,
            date_publication=published_date,
            lat=lat,
            lng=lng,
            tags=tags
        )
        return HttpResponse()
    else:
        return HttpResponse(status=404)

def get_a_news(request, id):
    news = get_object_or_404(Nouvelle, id=id)
    #retourner en JSON