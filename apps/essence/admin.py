from django.contrib import admin
from apps.essence.models import Station, Prix_station, Historique

admin.site.register(Station)
admin.site.register(Prix_station)
admin.site.register(Historique)