from datetime import datetime
from django.core.exceptions import MultipleObjectsReturned, ObjectDoesNotExist
from tastypie.http import HttpMultipleChoices, HttpGone
from tastypie.resources import ModelResource
from apps.essence.models import Station, Prix_station
from tastypie import fields
from django.conf.urls.defaults import url

class PrixStationResource(ModelResource):
    class Meta:
        queryset = Prix_station.objects.all()
        resource_name = 'essence/prixstation'
        ordering = ['date']

    def get_object_list(self, request):
        return super(PrixStationResource, self).get_object_list(request).order_by('date')

class StationResource(ModelResource):
    class Meta:
        queryset = Station.objects.all()
        resource_name = 'essence/station'
        list_allowed_methods = ['get']

    def dehydrate(self, bundle):
        if bundle.request.path == '/essence/station/':
            prix_station_resource = PrixStationResource()
            try:
                bundle.data['dernier_prix'] = prix_station_resource.get_object_list(bundle.request).filter(station=bundle.data['id']).order_by('-date')[0].prix
            except Exception, e:
                bundle.data['dernier_prix'] = 0
            bundle.data['historique'] = bundle.data['resource_uri'] + 'historique/'
        return bundle

    def override_urls(self):
        return [
            url(r'^(?P<resource_name>%s)/(?P<pk>\w[\w/-]*)/historique' % self._meta.resource_name, self.wrap_view('get_historique'), name="api_get_historique")
        ]

    def get_historique(self, request, **kwargs):
        try:
            obj = self.obj_get(request=request, **self.remove_api_resource_names(kwargs))
        except ObjectDoesNotExist:
            return HttpGone()
        except MultipleObjectsReturned:
            return HttpMultipleChoices("More than one resource is found at this URI.")
        prix_station_resource = PrixStationResource()
        return prix_station_resource.get_list(request, station=obj)
