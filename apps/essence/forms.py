from django import forms
from django.core.exceptions import ValidationError

class DynForm(forms.Form):
    """
    Dynamic form that allows the user to change and then verify the data that was parsed

    """
    def setFields(self, kwds):
        """
        Set the fields in the form
        """
        keys = kwds.keys()
        keys.sort()
        for k in keys:
            self.fields[k] = kwds[k]

    def setData(self, kwds):
        """
        Set the data to include in the form
        """
        keys = kwds.keys()
        keys.sort()
        for k in keys:
            self.data[k] = kwds[k]

    def validate(self, post):
        """
        Validate the contents of the form
        """
        for name,field in self.fields.items():
            try:
                field.clean(post[name])
            except ValidationError, e:
                self.errors[name] = e.messages

class StationForm(forms.Form):
    id = forms.IntegerField(widget=forms.HiddenInput)
    nom = forms.CharField(max_length=50)
    photo = forms.ImageField()

class PrixForm(forms.Form):
    id = forms.IntegerField(widget=forms.HiddenInput)
    prix = forms.FloatField()
