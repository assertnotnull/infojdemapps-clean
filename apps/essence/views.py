# -=- coding: utf-8 -=-
from datetime import datetime
import logging
import re
from django.contrib.auth import logout
from django.contrib.auth.forms import AuthenticationForm
from django.core.exceptions import ObjectDoesNotExist
from django.core.serializers import json
from django.http import HttpResponse
from django.shortcuts import render_to_response, render
from apps.essence.forms import StationForm, PrixForm
from apps.essence.logindecorator import login_required
from apps.essence.models import Station, Prix_station, record_signal, Historique
from apps.essence.yellowapi import YellowAPI

logger = logging.getLogger(__name__)

def trouver_station_proche(request, lat, lng):
    urltest = 'http://api.yellowapi.com/FindBusiness/?what=gas station&lang=en&where=cZ-73.554242%2C45.508867&pgLen=10&apikey=a1s2d3f4g5h6j7k8l9k6j5j4&UID=123456789'
    key = 'a1s2d3f4g5h6j7k8l9k6j5j4'
    yellowapi = YellowAPI(key, False, 'JSON')
    jsonstring = yellowapi.find_business('gas station', 'cZ%s,%s' % (lng,lat),'123456789',page_len=5)
    jsondata = json.simplejson.loads(jsonstring)
    listings = jsondata['listings']
    if jsondata['summary']['Prov'] == 'QC':
        for stationjson in listings:
            try:
                station = Station.objects.get(codepostal=stationjson['address']['pcode'])
            except ObjectDoesNotExist, e:
                station = Station()
            station.codepostal = stationjson['address']['pcode']
            station.nom = stationjson['name']
            station.lat = stationjson['geoCode']['latitude']
            station.lng = stationjson['geoCode']['longitude']
            station.rue = stationjson['address']['street']
            station.ville = stationjson['address']['city']
            station.save()
        return HttpResponse('woot')

    return HttpResponse(jsonstring, mimetype='application/json')

def index(request):
    return render_to_response('carte.html')

def mise_a_jour_prix(request):
    return render_to_response('update.html')

@login_required
def updatepage(request):
    if request.method == 'POST':
        if 'submit_prix' in request.POST:
            logger.debug('Le form est prix')
            formprix = PrixForm(request.POST)
            logger.debug('Valeurs affectes au form')
            if formprix.is_valid():
                logger.debug('Le form est valide')
                prix_station = Prix_station(
                    station=Station.objects.get(id=formprix.cleaned_data['id']),
                    prix=formprix.cleaned_data['prix'],
                    date=datetime.today()
                )
                prix_station.save()
                record_signal.send(sender=None, user=request.user)
                logger.debug('Objet prix cree')
            logger.debug(formprix.errors)
            formstation = StationForm()
        elif 'submit_station' in request.POST:
            logger.debug('Le form est station')
            formstation = StationForm(request.POST, request.FILES)
            if formstation.is_valid():
                station = Station.objects.get(id=formstation.cleaned_data['id'])
                station.nom = formstation.cleaned_data['nom']
                slugnom = slugify(formstation.cleaned_data['nom']) + '.' + request.FILES['photo'].name.split('.')[-1]
                station.photo.save(slugnom, request.FILES['photo'])
                station.save()
                record_signal.send(sender=None, user=request.user)
            formprix = PrixForm()
    else:
        logger.debug('Creation des forms')
        formstation = StationForm()
        formprix = PrixForm()
    return render(request, 'update.html', {'formstation': formstation, 'formprix': formprix})

def record_update(utilisateur, action, table, id, vieille_valeur, nouvelle_valeur):
    if table is 'station':
        historique = Historique(
            utilisateur = utilisateur,
            action = action,
            table_station_id = id,
            date_modification = datetime.now(),
            vieille_valeur = vieille_valeur,
            nouvelle_valeur = nouvelle_valeur
        )
    elif table is 'prix_station':
        historique = Historique(
            utilisateur = utilisateur,
            action = action,
            table_prix_id = id,
            date_modification = datetime.now(),
            vieille_valeur = vieille_valeur,
            nouvelle_valeur = nouvelle_valeur
        )

def slugify(inStr):
    removelist = ["a", "an", "as", "at", "before", "but", "by", "for","from","is", "in", "into", "like", "of", "off",
                  "on", "onto","per","since", "than", "the", "this", "that", "to", "up", "via","with",
                  'le', 'la', 'l','les','d']
    for a in removelist:
        aslug = re.sub(r'\b'+a+r'\b','',inStr)
    aslug = replace(aslug, [u'è',u'é'], 'e')
    aslug = replace(aslug, [u'à'], 'a')
    aslug = replace(aslug, [u'û'], 'u')
    aslug = replace(aslug, [u'ç'], 'c')
    aslug = re.sub('[^\w\s-]', '', aslug).strip().lower()
    aslug = re.sub('\s+', '-', aslug)
    return aslug

def replace(slug, toreplace, char):
    for a in toreplace:
        slug = slug.replace(a, char)
    return slug