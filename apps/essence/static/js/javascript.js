var geocoder = new google.maps.Geocoder();
var montreal = new google.maps.LatLng(45.55, -73.6); // begin centered on Montréal
var map;

function success(position) {
    var s = document.querySelector('#status');
    var latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
    var location = new google.maps.Marker({
        position: latlng,
        map: map
    });

    map.setOptions({zoom: 14});

    var resultat = "Position détectée: <br/>latitude: " + position.coords.latitude
        + "<br/> longitude: " + position.coords.longitude;
    var urlupdate = "http://localhost:8000/essence/trouver/" + position.coords.longitude + '/' + position.coords.latitude;
    var urlhtml = "<a href='"+ urlupdate + "'>update</a>";

    $("#status").html(resultat + urlhtml);

    //recherche par location.. retourne des details de la position
    geocoder.geocode({'latLng': latlng}, function(results, status) {
        var i;
        if (status == google.maps.GeocoderStatus.OK && results) {
            map.setCenter(results[0].geometry.location);
        } else {
            console.log("Erreur :" + status);
        }
    });
}

function error(msg) {
    console.log("abort");
    var s = document.querySelector('#status');
    s.innerHTML = typeof msg == 'string' ? msg : "failed";
    s.className = 'fail';
}

$(document).ready(function() {
    var options = {
        disableDefaultUI: false,
        zoom: 10,
        center: montreal,
        mapTypeId: google.maps.MapTypeId.TERRAIN,
        mapTypeControl: true,
        scaleControl: true,
        zoomControl: true,
        zoomControlOptions: {
            style: google.maps.ZoomControlStyle.LARGE,
            position: google.maps.ControlPosition.TOP_LEFT
        }
    }
    map = new google.maps.Map(document.getElementById("map_canvas"), options);
    //retourner toutes les stations
    $.getJSON('http://localhost:8000/essence/station/?format=json&limit=0',
        function(data) {
            newsArticlesForMap = data;
            createEssenceMarkers(data);
        });

    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(success, error);
    } else {
        $("#status").html('Impossible de détecter la location');
    }
});

function createEssenceMarkers(articleMarkers) {
    var i;
    var data = articleMarkers['objects'];
    infowindow = new google.maps.InfoWindow({
        maxWidth: 300
    });
    for (i in data) {
        var size = new google.maps.Size(32, 37, "px", "px"); // size of icon
        var lat = data[i]["lat"];
        var lng = data[i]["lng"];
        var marker = new google.maps.Marker({
//            icon: new google.maps.MarkerImage("icons.png", size, 32, null, null), // null, null on MarkerImage are for anchor and scaledSize
            position: new google.maps.LatLng(lat, lng),
            rowid: i,
            lat: lat,
            lng: lng,
            id: data[i]["id"],
            nom: data[i]["nom"],
            dernier_prix: data[i]["dernier_prix"],
            photo: data[i]["photo"],
            map: map
        });

        google.maps.event.addListener(marker, "click", function(event) {

            var infoHtml = "<img src='" + this.photo + "'/><p class='infoboxTitle' id='nomStation'>" + this.nom +
                "</p><p class='infoboxContentSnippet' id='prixStation'>" + this.dernier_prix + "$/L";
            infowindow.setContent(infoHtml);
            infowindow.open(map, this);
        });
    }
}