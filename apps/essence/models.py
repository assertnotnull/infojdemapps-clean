from datetime import datetime
from django.contrib.auth.models import User
from django.db import models
import math
from django.db.models.signals import post_save
from django.dispatch.dispatcher import receiver, Signal

record_signal = Signal(providing_args=['user', 'id', 'table', 'vieille_valeur', 'nouvelle_valeur'])

class Station(models.Model):
    codepostal = models.CharField(max_length=6, unique=True, db_index=True)
    nom = models.CharField(max_length=50)
    photo = models.ImageField(upload_to='essence/photos/', blank=True)
    lat = models.FloatField('latitude')
    lng = models.FloatField('longitude')
    rue = models.CharField(max_length=100)
#    arrondissement = models.CharField(max_length=100, null=True)
    ville = models.CharField(max_length=100, db_index=True)

    def __unicode__(self):
        return self.nom

    def haversineDistance(self, lat, lng):
        latdistance = math.radians(lat - self.lat)
        lngdistance = math.radians(lng - self.lng)
        a = math.sin(latdistance/2) * math.sin(latdistance/2) +\
            math.cos(math.radians(lat)) * math.cos(math.radians(self.lat)) *\
            math.sin(lngdistance/2) * math.sin(lngdistance/2)
        c = 2 * math.atan2(math.sqrt(a), math.sqrt(1- a))
        return c * 6371.0

class Prix_station(models.Model):
    station = models.ForeignKey(Station)
    date = models.DateField()
    prix = models.FloatField()

    class Meta:
        unique_together = ('station', 'date')

    def __unicode__(self):
        return '%s %s' % (self.date, self.prix)

#    def record_change_for_user(self, **kwargs):
#        station_signal.send(kwargs)

#@receiver(post_save, sender=Prix_station)
def record(sender, **kwargs):
    print 'user is: %s' % kwargs['user']


record_signal.connect(record)
#    if 'created' in kwargs:
#        obj = kwargs['instance']
#        Historique.objects.create(
#            user=user,
#            action='mise a jour',
#            table_prix_id=id,
#            date_modification=datetime.now
#        )
#
class Historique(models.Model):
    utilisateur = models.OneToOneField(User)
    action = models.CharField(max_length=200)
    table_prix_station_id = models.ForeignKey(Prix_station, null=True)
    table_station_id = models.ForeignKey(Station, null=True)
    date_modification = models.DateTimeField()
    vieille_valeur = models.CharField(max_length=50, blank=True)
    nouvelle_valeur = models.CharField(max_length=50, blank=True)