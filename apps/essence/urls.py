from django.conf.urls.defaults import patterns, url, include
from apps.essence.api import StationResource, PrixStationResource

station_resource = StationResource()
prixstation_resource = PrixStationResource()

urlpatterns = patterns('',
    url(r'', include(station_resource.urls), name='station_url'),
    url(r'', include(prixstation_resource.urls), name='prix_station_url'),
    url(r'^essence/mettreajour/?$', 'apps.essence.views.updatepage', name='update_page_url'),
    url(r'^essence/post/?$', 'apps.essence.views.mise_a_jour_prix', name='update_url'),
    url(r'^essence/trouver/(?P<lng>[0-9.-]+)/(?P<lat>[0-9.-]+)/$', 'apps.essence.views.trouver_station_proche', name='trouver_station_url'),
    url(r'^essence/?$', 'apps.essence.views.index', name='index'),
    url(r'^accounts/', include('registration.backends.default.urls')),
)