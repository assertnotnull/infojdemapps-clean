from functools import wraps  # adapt if you need python 2.4 support

from django.contrib.auth import REDIRECT_FIELD_NAME
from django.contrib.auth.views import login


def login_required(view_callable):
    def check_login(request, *args, **kwargs):
        if request.user.is_authenticated():
            return view_callable(request, *args, **kwargs)
        assert hasattr(request, 'session'), "Session middleware needed."
        login_kwargs = {
            'extra_context': {
                REDIRECT_FIELD_NAME: request.get_full_path(),
                },
            }
        return login(request, **login_kwargs)
    return check_login